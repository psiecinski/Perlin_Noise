   /** SIMPLEX 
           source: https://github.com/joshforisha/fast-simplex-noise-js/blob/master/main.js
           **/
   function FastSimplexNoise(a) {
       if (a || (a = {}), this.amplitude = a.amplitude || 1, this.frequency = a.frequency || 1, this.octaves =
           parseInt(a.octaves || 1), this.persistence = a.persistence || .5, this.random = a.random || Math.random,
           "number" == typeof a.min && "number" == typeof a.max)
           if (a.min >= a.max) console.error("options.min must be less than options.max");
           else {
               var b = parseFloat(a.min),
                   c = parseFloat(a.max),
                   d = c - b;
               this.scale = function (a) {
                   return b + (a + 1) / 2 * d
               }
           }
       else this.scale = function (a) {
           return a
       };
       var e, f = new Uint8Array(256);
       for (e = 0; e < 256; e++) f[e] = e;
       var g, h;
       for (e = 255; e > 0; e--) g = Math.floor((e + 1) * this.random()), h = f[e], f[e] = f[g], f[g] = h;
       for (this.perm = new Uint8Array(512), this.permMod12 = new Uint8Array(512), e = 0; e < 512; e++) this.perm[e] =
           f[255 & e], this.permMod12[e] = this.perm[e] % 12
   }

   function dot2D(a, b, c) {
       return a[0] * b + a[1] * c
   }

   function dot3D(a, b, c, d) {
       return a[0] * b + a[1] * c + a[2] * d
   }

   function dot4D(a, b, c, d, e) {
       return a[0] * b + a[1] * c + a[2] * d + a[3] * e
   }
   var G2 = (3 - Math.sqrt(3)) / 6,
       G3 = 1 / 6,
       G4 = (5 - Math.sqrt(5)) / 20,
       GRAD3 = [
           [1, 1, 0],
           [-1, 1, 0],
           [1, -1, 0],
           [-1, -1, 0],
           [1, 0, 1],
           [-1, 0, 1],
           [1, 0, -1],
           [-1, 0, -1],
           [0, 1, 1],
           [0, -1, -1],
           [0, 1, -1],
           [0, -1, -1]
       ],
       GRAD4 = [
           [0, 1, 1, 1],
           [0, 1, 1, -1],
           [0, 1, -1, 1],
           [0, 1, -1, -1],
           [0, -1, 1, 1],
           [0, -1, 1, -1],
           [0, -1, -1, 1],
           [0, -1, -1, -1],
           [1, 0, 1, 1],
           [1, 0, 1, -1],
           [1, 0, -1, 1],
           [1, 0, -1, -1],
           [-1, 0, 1, 1],
           [-1, 0, 1, -1],
           [-1, 0, -1, 1],
           [-1, 0, -1, -1],
           [1, 1, 0, 1],
           [1, 1, 0, -1],
           [1, -1, 0, 1],
           [1, -1, 0, -1],
           [-1, 1, 0, 1],
           [-1, 1, 0, -1],
           [-1, -1, 0, 1],
           [-1, -1, 0, -1],
           [1, 1, 1, 0],
           [1, 1, -1, 0],
           [1, -1, 1, 0],
           [1, -1, -1, 0],
           [-1, 1, 1, 0],
           [-1, 1, -1, 0],
           [-1, -1, 1, 0],
           [-1, -1, -1, 0]
       ];
   "undefined" != typeof module && (module.exports = FastSimplexNoise), FastSimplexNoise.prototype.cylindrical2D =
       function (a, b, c) {
           var d = b / a,
               e = a / (2 * Math.PI),
               f = 2 * d * Math.PI,
               g = e * Math.sin(f),
               h = e * Math.cos(f);
           return this.in3D(g, h, c)
       }, FastSimplexNoise.prototype.cylindrical3D = function (a, b, c, d) {
           var e = b / a,
               f = a / (2 * Math.PI),
               g = 2 * e * Math.PI,
               h = f * Math.sin(g),
               i = f * Math.cos(g);
           return this.in4D(h, i, c, d)
       }, FastSimplexNoise.prototype.in2D = function (a, b) {
           for (var c = this.amplitude, d = this.frequency, e = 0, f = 0, g = this.persistence, h = 0; h < this.octaves; h++)
               f += this.raw2D(a * d, b * d) * c, e += c, c *= g, d *= 2;
           var i = f / e;
           return this.scale(i)
       }, FastSimplexNoise.prototype.in3D = function (a, b, c) {
           for (var d = this.amplitude, e = this.frequency, f = 0, g = 0, h = this.persistence, i = 0; i < this.octaves; i++)
               g += this.raw3D(a * e, b * e, c * e) * d, f += d, d *= h, e *= 2;
           var j = g / f;
           return this.scale(j)
       }, FastSimplexNoise.prototype.in4D = function (a, b, c, d) {
           for (var e = this.amplitude, f = this.frequency, g = 0, h = 0, i = this.persistence, j = 0; j < this.octaves; j++)
               h += this.raw4D(a * f, b * f, c * f, d * f) * e, g += e, e *= i, f *= 2;
           var k = h / g;
           return this.scale(k)
       }, FastSimplexNoise.prototype.raw2D = function (a, b) {
           var e, f, g, p, q, c = this.perm,
               d = this.permMod12,
               h = .5 * (a + b) * (Math.sqrt(3) - 1),
               i = Math.floor(a + h),
               j = Math.floor(b + h),
               k = (i + j) * G2,
               l = i - k,
               m = j - k,
               n = a - l,
               o = b - m;
           n > o ? (p = 1, q = 0) : (p = 0, q = 1);
           var r = n - p + G2,
               s = o - q + G2,
               t = n - 1 + 2 * G2,
               u = o - 1 + 2 * G2,
               v = 255 & i,
               w = 255 & j,
               x = d[v + c[w]],
               y = d[v + p + c[w + q]],
               z = d[v + 1 + c[w + 1]],
               A = .5 - n * n - o * o;
           A < 0 ? e = 0 : (A *= A, e = A * A * dot2D(GRAD3[x], n, o));
           var B = .5 - r * r - s * s;
           B < 0 ? f = 0 : (B *= B, f = B * B * dot2D(GRAD3[y], r, s));
           var C = .5 - t * t - u * u;
           return C < 0 ? g = 0 : (C *= C, g = C * C * dot2D(GRAD3[z], t, u)), 70.14805770654148 * (e + f + g)
       }, FastSimplexNoise.prototype.raw3D = function (a, b, c) {
           var f, g, h, i, u, v, w, x, y, z, d = this.perm,
               e = this.permMod12,
               j = (a + b + c) / 3,
               k = Math.floor(a + j),
               l = Math.floor(b + j),
               m = Math.floor(c + j),
               n = (k + l + m) * G3,
               o = k - n,
               p = l - n,
               q = m - n,
               r = a - o,
               s = b - p,
               t = c - q;
           r >= s ? s >= t ? (u = 1, v = 0, w = 0, x = 1, y = 1, z = 0) : r >= t ? (u = 1, v = 0, w = 0, x = 1, y = 0,
               z = 1) : (u = 0, v = 0, w = 1, x = 1, y = 0, z = 1) : s < t ? (u = 0, v = 0, w = 1, x = 0, y = 1, z =
               1) : r < t ? (u = 0, v = 1, w = 0, x = 0, y = 1, z = 1) : (u = 0, v = 1, w = 0, x = 1, y = 1, z = 0);
           var A = r - u + G3,
               B = s - v + G3,
               C = t - w + G3,
               D = r - x + 2 * G3,
               E = s - y + 2 * G3,
               F = t - z + 2 * G3,
               G = r - 1 + 3 * G3,
               H = s - 1 + 3 * G3,
               I = t - 1 + 3 * G3,
               J = 255 & k,
               K = 255 & l,
               L = 255 & m,
               M = e[J + d[K + d[L]]],
               N = e[J + u + d[K + v + d[L + w]]],
               O = e[J + x + d[K + y + d[L + z]]],
               P = e[J + 1 + d[K + 1 + d[L + 1]]],
               Q = .5 - r * r - s * s - t * t;
           Q < 0 ? f = 0 : (Q *= Q, f = Q * Q * dot3D(GRAD3[M], r, s, t));
           var R = .5 - A * A - B * B - C * C;
           R < 0 ? g = 0 : (R *= R, g = R * R * dot3D(GRAD3[N], A, B, C));
           var S = .5 - D * D - E * E - F * F;
           S < 0 ? h = 0 : (S *= S, h = S * S * dot3D(GRAD3[O], D, E, F));
           var T = .5 - G * G - H * H - I * I;
           return T < 0 ? i = 0 : (T *= T, i = T * T * dot3D(GRAD3[P], G, H, I)), 94.68493150681972 * (f + g + h + i)
       }, FastSimplexNoise.prototype.raw4D = function (a, b, c, d) {
           var f, g, h, i, j, e = this.perm,
               k = (a + b + c + d) * (Math.sqrt(5) - 1) / 4,
               l = Math.floor(a + k),
               m = Math.floor(b + k),
               n = Math.floor(c + k),
               o = Math.floor(d + k),
               p = (l + m + n + o) * G4,
               q = l - p,
               r = m - p,
               s = n - p,
               t = o - p,
               u = a - q,
               v = b - r,
               w = c - s,
               x = d - t,
               y = 0,
               z = 0,
               A = 0,
               B = 0;
           u > v ? y++ : z++, u > w ? y++ : A++, u > x ? y++ : B++, v > w ? z++ : A++, v > x ? z++ : B++, w > x ? A++ :
               B++;
           var C, D, E, F, G, H, I, J, K, L, M, N;
           C = y >= 3 ? 1 : 0, D = z >= 3 ? 1 : 0, E = A >= 3 ? 1 : 0, F = B >= 3 ? 1 : 0, G = y >= 2 ? 1 : 0, H = z >=
               2 ? 1 : 0, I = A >= 2 ? 1 : 0, J = B >= 2 ? 1 : 0, K = y >= 1 ? 1 : 0, L = z >= 1 ? 1 : 0, M = A >= 1 ?
               1 : 0, N = B >= 1 ? 1 : 0;
           var O = u - C + G4,
               P = v - D + G4,
               Q = w - E + G4,
               R = x - F + G4,
               S = u - G + 2 * G4,
               T = v - H + 2 * G4,
               U = w - I + 2 * G4,
               V = x - J + 2 * G4,
               W = u - K + 3 * G4,
               X = v - L + 3 * G4,
               Y = w - M + 3 * G4,
               Z = x - N + 3 * G4,
               $ = u - 1 + 4 * G4,
               _ = v - 1 + 4 * G4,
               aa = w - 1 + 4 * G4,
               ba = x - 1 + 4 * G4,
               ca = 255 & l,
               da = 255 & m,
               ea = 255 & n,
               fa = 255 & o,
               ga = e[ca + e[da + e[ea + e[fa]]]] % 32,
               ha = e[ca + C + e[da + D + e[ea + E + e[fa + F]]]] % 32,
               ia = e[ca + G + e[da + H + e[ea + I + e[fa + J]]]] % 32,
               ja = e[ca + K + e[da + L + e[ea + M + e[fa + N]]]] % 32,
               ka = e[ca + 1 + e[da + 1 + e[ea + 1 + e[fa + 1]]]] % 32,
               la = .5 - u * u - v * v - w * w - x * x;
           la < 0 ? f = 0 : (la *= la, f = la * la * dot4D(GRAD4[ga], u, v, w, x));
           var ma = .5 - O * O - P * P - Q * Q - R * R;
           ma < 0 ? g = 0 : (ma *= ma, g = ma * ma * dot4D(GRAD4[ha], O, P, Q, R));
           var na = .5 - S * S - T * T - U * U - V * V;
           na < 0 ? h = 0 : (na *= na, h = na * na * dot4D(GRAD4[ia], S, T, U, V));
           var oa = .5 - W * W - X * X - Y * Y - Z * Z;
           oa < 0 ? i = 0 : (oa *= oa, i = oa * oa * dot4D(GRAD4[ja], W, X, Y, Z));
           var pa = .5 - $ * $ - _ * _ - aa * aa - ba * ba;
           return pa < 0 ? j = 0 : (pa *= pa, j = pa * pa * dot4D(GRAD4[ka], $, _, aa, ba)), 72.37857097679466 * (f +
               g + h + i + j)
       }, FastSimplexNoise.prototype.spherical2D = function (a, b, c) {
           var d = b / a,
               e = c / a,
               f = 2 * d * Math.PI,
               g = e * Math.PI,
               h = Math.sin(g + Math.PI),
               i = 2 * Math.PI,
               j = i * Math.sin(f) * h,
               k = i * Math.cos(f) * h,
               l = i * Math.cos(g);
           return this.in3D(j, k, l)
       }, FastSimplexNoise.prototype.spherical3D = function (a, b, c, d) {
           var e = b / a,
               f = c / a,
               g = 2 * e * Math.PI,
               h = f * Math.PI,
               i = Math.sin(h + Math.PI),
               j = 2 * Math.PI,
               k = j * Math.sin(g) * i,
               l = j * Math.cos(g) * i,
               m = j * Math.cos(h);
           return this.in4D(k, l, m, d)
       };
 var text = [{
         hexidec: "#25a9cb"
     },
     // {
     //     hexidec: "#28aed0"
     // },
     {
         hexidec: "#0d9928"
     },
     {
         hexidec: "#32b7d8"
     },
     // { hexidec: "#3dc2e3" },
     // { hexidec: "#4ccbeb" },
     // { hexidec: "#57d8f8" },
     // { hexidec: "6ee0fc" },
     // { hexidec: "#77e3fd" },
     // { hexidec: "#b1e1d1" },
     // { hexidec: "#cae1b1" },
     {
         hexidec: "#ebe8b5"
     },
     {
         hexidec: "#e3dfa1"
     }
 ]
noiseGen = new FastSimplexNoise({
    amplitude: 22.015,
    frequency: 0.005,
    octaves: 5,
    max: text.length,
    min: 0
});